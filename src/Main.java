import java.time.Duration;
import java.time.Instant;

public class Main {
    public static void main(String[] args) {
        Instant start = Instant.now();
        int numberOfDivisors = 0;
        double number = 0;
        for (int e1 = 0; e1 <= 16; e1++) {
            for (int e2 = 0; e2 <= 10; e2++) {
                if (e2 > e1) {
                    break;
                } else {
                    for (int e3 = 0; e3 <= 7; e3++) {
                        if (e3 > e2) {
                            break;
                        } else {
                            for (int e4 = 0; e4 <= 5; e4++) {
                                if (e4 > e3) {
                                    break;
                                }
                                for (int e5 = 0; e5 <= 4; e5++) {
                                    if (e5 > e4) {
                                        break;
                                    } else {
                                        for (int e6 = 0; e6 <= 4; e6++) {
                                            if (e6 > e5) {
                                                break;
                                            } else {
                                                if (Math.pow(2, e1) * Math.pow(3, e2) * Math.pow(5, e3) * Math.pow(7, e4) * Math.pow(11, e5) * Math.pow(13, e6) < 100000) {
                                                    if ((e1 + 1) * (e2 + 1) * (e3 + 1) * (e4 + 1) * (e5 + 1) * (e6 + 1) > numberOfDivisors) {
                                                        number = Math.pow(2, e1) * Math.pow(3, e2) * Math.pow(5, e3) * Math.pow(7, e4) * Math.pow(11, e5) * Math.pow(13, e6);
                                                        numberOfDivisors = (e1 + 1) * (e2 + 1) * (e3 + 1) * (e4 + 1) * (e5 + 1) * (e6 + 1);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(number);
        Instant finish = Instant.now();
        System.out.println(Duration.between(start, finish).toMillis());
    }

}
